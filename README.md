# Sample Worker Service
## About
This is a sample gRPC Worker Service. In real world this would be the actual microservice doing the work.


## Key Points

- Kubernetes resources can be found in resources/kubernetes directory.
- Kubernetes Service is a clusterIP service.