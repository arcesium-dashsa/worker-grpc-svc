package com.satya.grpc_sample

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GrpcSampleApplication

fun main(args: Array<String>) {
    runApplication<GrpcSampleApplication>(*args)
}
