package com.satya.grpc_sample.config

import kotlinx.coroutines.reactive.publish
import org.springframework.context.annotation.Configuration
import java.util.*

@Configuration
class Identifier {
    val id = UUID.randomUUID().toString()
}