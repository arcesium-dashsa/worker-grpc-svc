package com.satya.grpc_sample.controller

import com.satya.grpc_sample.config.Identifier
import com.satya.samplesvc.Request
import com.satya.samplesvc.Response
import com.satya.samplesvc.SampleSvcGrpcKt
import net.devh.boot.grpc.server.service.GrpcService

@GrpcService
class SampleGRPCController(private val identifier: Identifier) : SampleSvcGrpcKt.SampleSvcCoroutineImplBase() {
    override suspend fun greet(request: Request): Response {
        return Response.newBuilder()
                .setResponse("Hello there from ${identifier.id}").build()
    }
}